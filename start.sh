#!/bin/sh

ROOT=$(pwd)

cd builddocker

docker build -t="javabuild" .
docker run -v $ROOT:/builddir --rm javabuild

cd $ROOT/rundocker

docker build -t="java_webapp" .

#docker run -d -p 8080:8080 java_webapp