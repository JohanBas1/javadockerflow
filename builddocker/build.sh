#!/bin/sh

cd /builddir/

# fix certificate problem see:
# https://github.com/docker-library/java/issues/19
/var/lib/dpkg/info/ca-certificates-java.postinst configure

./gradlew build

cp /builddir/build/libs/*.war /builddir/rundocker/
